#!/bin/sh

sudo systemctl daemon-reload
sudo systemctl enable seekware-fbdev.service
sudo systemctl start seekware-fbdev.service
sudo systemctl status seekware-fbdev.service | cat

cd images
rm *.png

echo "wait 4s"
sleep 4 
echo "start"

x=1
while [ $x -le $1 ]
do
	echo img$x.png
	fbgrab img$x.png
	convert -crop 320x240+0+0 img$x.png +repage img$x.png
	sleep 0
	x=$(( $x + 1 ))
done

echo "stop"
sudo systemctl stop seekware-fbdev.service
sudo systemctl disable seekware-fbdev.service
