#!/bin/sh

echo "Number of images to capture (f ~= 1Hz): "
read Nimg

echo "stitching mode (scans | panorama): "
read MOD

sudo systemctl daemon-reload
sudo systemctl enable seekware-fbdev.service
sudo systemctl start seekware-fbdev.service
sudo systemctl status seekware-fbdev.service | cat

cd ./seekware/images/
rm *.png

echo "starting seekware service"
sleep 4 

x=1
while [ $x -le $Nimg ]
do
	echo capturing img$x.png
	fbgrab img$x.png
	convert -crop 320x240+0+0 img$x.png +repage img$x.png
	sleep 0
	x=$(( $x + 1 ))
done

echo "stopping seekware service"
sudo systemctl stop seekware-fbdev.service
sudo systemctl disable seekware-fbdev.service
echo "stitching images"

cd ../../OCVstitching/build
./stitching --mode $MOD ../../seekware/images/*.png
cd ../..
cp ./OCVstitching/build/result.jpg ./thermal.jpg
convert thermal.jpg -canny 0x1+10%+30% gradient.jpg
cp ./*.jpg /var/www/html/site/images/
